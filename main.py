"""
Nom ......... : main.py
Role ........ : Fonction principale permettant de lancer le scan des appareils
Auteur ...... : Onésime
Version ..... : V1.1 du 31/3/2017
"""


### Import
from src.alerteur import AlerteurDiscoverer
from src.loader import Loader
from src.web_loader import WebLoader

from src.capteur import Capteur

"""
	Main: lance le système
"""

### Utiliser ainsi pour que pydoc ne lance pas le main
if __name__ == "__main__":
	while True:
		try:
			### fichier de settings
			filename = "settings.ini"

			### Charge la configuration
			loader = Loader()
			configuration = loader.loadConfig(filename)

			### Pour le WebLoader, mettre le server en running préalablement
			"""
			webloader = WebLoader()
			configuration = webloader.LoadConfigFromServer('http://localhost:5000/configuration')
			"""

			### Scanne
			alerteur = AlerteurDiscoverer(configuration)
			alerteur.scanne()
		except Exception as exept:
			print(exept)













