"""
Nom ......... : mailer.py
Role ........ : Classe permettant d'envoyer 1 mail selon 1 capteur, en utilisant 1 fichier de template
Auteur ...... : Onésime
Version ..... : V1.1 du 31/3/2017
"""
### import
import smtplib

# Import les modules email
### HTML send as mail ...
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

### template le mail
from datetime import datetime,timedelta

### 
from jinja2 import Template
from jinja2 import Environment, FileSystemLoader

import re
import time

### Permet d'envoyer 1 mail
class Mailer:
	"""
		Classe qui permet d'envoyer 1 mail via 1 serveur smtp
	"""
	### init
	def __init__(self, configuration):
		"""
			Constructeur qui permet d'utiliser 1 dictionnaire de configuration pour construire les attributs
			Veuillez modifier le fichier templates/message.html pour modifier le mail émis
		"""
		### Expéditeur
		self._mail_expediteur = "centrale.alerte.grp.1@gmail.com"
		self._mdp_expediteur = "raspberry"

		### env
		self._env = Environment()
		self._env.loader = FileSystemLoader('templates')
		
		### Destinataire
		destinataire = configuration["data"].get("dest")
		if destinataire == None:
			exit()
		else:
			### check mail
			is_ok = re.match(r"^[a-z0-9._-]+@[a-z0-9_-]+\.[(com|fr)]+", destinataire)
			if is_ok == None:
				self._mail_destinataire = None
				self._template_filename = "desterror.html"
				self.sendMail(destinataire)
			else:
				self._mail_destinataire = destinataire

		### template final
		self._template_filename = "message.html"

	### Action principale
	def sendMail(self, capteur):
		"""
			Fonction principale qui prend 1 capteur en paramétre et envois 1 mail selon le capteur
		"""
		while True:
			try:
				### Check
				if capteur == None:
					return None

				dest = ""
				### Envois anyway
				if self._mail_destinataire == None:
					dest = self._mail_expediteur
				else:
					dest = self._mail_destinataire

				### Message
				message = MIMEMultipart()

				### Set le header
				message['From'] = self._mail_expediteur
				message['To'] = dest
				message['Subject'] = "Message de votre centrale d'alerte"

				### Contenue du message
				content = self.formateMail(capteur)

				### Attache le document text au message
				message.add_header('Content-Type','text/html')
				message.attach(MIMEText(content, "html"))

				### Utilise gmail pour émettre le mail
				mailserver = smtplib.SMTP('smtp.gmail.com', 587)

				### STMP protocole
				mailserver.ehlo()
				mailserver.starttls()

				### Ce login dans gmail
				mailserver.login(self._mail_expediteur, self._mdp_expediteur)
		
				### Emettre le mail
				mailserver.sendmail(self._mail_expediteur, dest, message.as_string())

				### quitter
				mailserver.quit()

				return
			except Exception as exept:
				### attend 5 sec avant de réessayer
				time.sleep(5)

	### formate 1 mail
	def formateMail(self, capteur):
		"""
			Permet de formater 1 mail en utilisant 1 fichier de template ( templates/message.html )
		"""
		### Build from template
		template = self._env.get_template(self._template_filename)
		
		content = template.render(
			title="Un capteur vient de bouger", 
			capteur=capteur)

		### return
		return content











