
"""
Nom ......... : test.py
Role ........ : Fichier de tests
Auteur ...... : Onésime
Version ..... : V1.1 du 3/4/2017
"""

### import
import pytest

from src.filtre import Filtre
from src.loader import Loader
from src.capteur import Capteur

### test loader
def test_loader():
	"""
		Permet de test le loader
	"""
	loader = Loader()
	dico = loader.LoadConfig("settings.ini")

	assert isinstance(dico, dict) == True
	assert len(dico) >= 2

### Filtre
def test_filtre():
	###
	loader = Loader()
	dico = loader.LoadConfig("settings.ini")

	### filtre
	filtre = Filtre(dico)

	capt = filtre.FiltrerCapteur("FA:12:27:A9:E4:80", "jaalee")

	assert capt != None
	assert isinstance(capt, Capteur) == True

### Test loading from server

### Fixture du serveur

from src.app import app
from src.web_loader import WebLoader
import json

@pytest.fixture
def client(request):
	### 
	test_client = app.test_client()
	return test_client

### Permet de charger from server
def test_web_loader(client):
	### Just get
	url = "http://localhost:5000/configuration"
	
	response = client.get(url)

	assert response.status_code == 200

	### Transforme en objet
	configuration = json.loads(response.data.decode('utf-8'))

	assert isinstance(configuration, dict) == True
	assert configuration.get("data") != None
	assert configuration.get("capteurs") != None




