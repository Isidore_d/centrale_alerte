"""
Nom ......... : web_loader.py
Role ........ : Permet de charger 1 configuration depuis 1 serveur distant
Auteur ...... : Onésime
Version ..... : V1.1 du 31/3/2017
"""

### Import
import requests
import json

### Permet de load from the web
class WebLoader:
	"""
		Classe permettant d'obtenir 1 configuration depuis 1 serveur distant
	"""
	### init
	def __init__(self):
		pass

	### Permet de load la config
	def loadConfigFromServer(self, url):
		"""
			Fonction principale permettant de charger 1 configuration depuis le serveur distant
			Une url est utiliser pour cela
		"""
		### Obtient le document
		response = requests.get(url)

		### Transforme en objet
		configuration = json.loads(response.text)

		### return
		return configuration
