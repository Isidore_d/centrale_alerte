"""
Nom ......... : alerteur.py
Role ........ : Scanne les appareils bluetooths à proximité et émet 1 mail pour ceux du système
Auteur ...... : Onésime
Version ..... : V1.1 du 31/3/2017
"""

### Import
import os
import signal
import subprocess
import time
import sys

from src.filtre import Filtre
from src.mailer import Mailer

class AlerteurDiscoverer():
	"""
		Classe permettant d'effectuer 1 scan d'appareils bluetooth
	"""
	def __init__(self, configuration):
		"""
			Constructeur.
			Initialise 1 Filtre et 1 Mailer
		"""
		### Construit le filtre
		self._filtre = Filtre(configuration)

		### Construit le mailer
		self._mailer = Mailer(configuration)
		
	def scanne(self, duration=5):
		"""
			Fonction principale
			Permet de scanner pour 1 durée indéterminée les appareils bluetooth à proximité
		"""
		while True:
			### Discoverer
			self.findDevices(duration)

	def findDevices(self, duration=5):
		"""
			Lance 1 fonction de recherche pour une durée de 'duration'
		"""
		print("findDevices")
		
		proc = subprocess.Popen(['/usr/bin/hcitool', 'lescan'], stdout=subprocess.PIPE)

		### sleep for duration
		time.sleep(duration)
		
		### kill subprocess
		os.kill(proc.pid, signal.SIGINT)

		### pass 1st
		proc.stdout.readline().decode("UTF-8")

		for lines in proc.stdout.readlines():
			### lines
			values = lines.decode("UTF-8").split(" ")

			### device discovered
			self.deviceDiscovered(values[0], values[1][:-1])

	def deviceDiscovered(self, address, name):
		"""
			Fonction appeller pour chaque appareil découvert à proximité
			A redéfinir pour modifier le comportement
		"""
		
		### Pour check
		print("%s - %s" % (address, name))

		### Check si on envois le mail
		capteur_retourner = self._filtre.filtrerDevice(address, name)

		### Fonction return None si tu ne doit pas envoyer de mail
		if capteur_retourner == None:
			### Do nothing
			return
		else:
			### Faire le mail et l'envoyer
			self._mailer.sendMail(capteur_retourner)



