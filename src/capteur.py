"""
Nom ......... : capteur.py
Role ........ : Classe représentant 1 capteur
Auteur ...... : Onésime
Version ..... : V1.1 du 31/3/2017
"""
### import
from datetime import datetime,timedelta
import re

### Class représent 1 capteur et sa configuration ( pas 1 capteur réel )
class Capteur:
	"""
		Classe représentant 1 capteur ( adresse mac, nom d'obtet, horodatage )
	"""

	#matcheur = re.compile(r"[A-F0-9]{12}")
	
	def __init__(self, adresse_mac, nom_objet="Capteur(unnamed)"):
		"""
			Constructeur qui prend en paramètre 1 adresse Mac valide et 1 nom d'objet facultatif
			Le constructeur initialise à None l'horodatage
		"""
		### to check
		self._adresse_MAC = adresse_mac

		### Nom "user friendly"
		self._nom_objet = nom_objet

		### Dernier date time
		self._timestamp = None

	### shortcut
	def setHorodatageToNow(self):
		"""
			Permet de modifier l'horodatage à la date de 'maintenant'
		"""
		self._timestamp = datetime.now()

	### Permet de retourner l'horodatage
	def getHorodatage(self):
		"""
			Retourne l'horodatage du capteur
		"""
		return self._timestamp

	### adresse mac
	def getAdresseMAC(self):
		"""
			Retourne l'adresse MAC du capteur
		"""
		return self._adresse_MAC

	### name
	def getName(self):
		"""
			Retourne le nom de l'objet auquel le capteur est coller
		"""
		return self._nom_objet

	### verify mac adresse
	def isMacValid(mac):
		"""
			Permet de véfifier si l'adresse mac est 1 adresse mac valide
		"""
		#is_ok = Capteur.matcheur.match(mac)
		#is_ok = re.match(r"[A-F0-9]{12}", mac)
		is_ok = re.match(r"((([A-F0-9]{2}):){5})[a-z0-9]{2}", mac)
		if is_ok == None:
			return False
		return True








