"""
Nom ......... : loader.py
Role ........ : Classe permettant de charger 1 fichier de configuration et d'obtenir 1 dictionnaire
Auteur ...... : Onésime
Version ..... : V1.1 du 31/3/2017
"""
### import
import configparser

### class permettant de loader
class Loader:
	"""
		Classe qui utilise 1 fichier de settings pour retourner 1 dictionnaire de configuration
	"""
	### init
	def __init__(self):
		pass

	### Load
	def loadConfig(self, filename):
		"""
			Fonction principale qui charge le fichier, et le transforme en un dictionnaire formatter
		"""
		### Check
		if filename == None:
			return None

		### try

		### load
		config = self._loadConfig(filename)

		### to dict
		dictionnary = self._configToDict(config)

		### Peut mettre du traitement ici ?

		return dictionnary

	### Load config
	def _loadConfig(self, filename):
		"""
			Charge un fichier
		"""
		config = configparser.ConfigParser()

		config.read(filename)

		return config

	### Config to dict
	def _configToDict(self, config):
		"""
			Transforme 1 config en dictionnaire
		"""
		result = {}
		for section in config.sections():
			result[section] = {}
			for key, val in config.items(section):
				result[section][key] = val
		return result







