"""
Nom ......... : filtre.py
Role ........ : Classe permettant de filtrer des appareils détecter et d'indiquer si on envois 1 mail ou non
Auteur ...... : Onésime
Version ..... : V1.1 du 31/3/2017
"""
### import
from datetime import datetime,timedelta

from src.capteur import Capteur

import json

### Class permettant de filtrer
class Filtre:
	"""
		Classe permettant de filtrer 1 adresse MAC et de retourner None ou 1 capteur selon des paramétres
		Contient la liste des capteurs du système
	"""
	
	def __init__(self, configuration):
		"""
			Construit la liste des capteurs selon 1 dictionnaire de configuration formatter
		"""
		### Paramétre en utilisant la configuration
		self.__capteurs = { }

		### fonction qui parse
		self._initFromConfiguration(configuration)

	### Retourne None si ne doit pas envoyer 1 mail, 1 capteur sinon
	def filtrerDevice(self, adresse_mac_detecter, name):
		"""
			Fonction principale qui retourne None ou 1 capteur selon sa présence, et son horodagage
		"""
		### Check ours
		capteur = self.__capteurs.get(str(adresse_mac_detecter))
		if capteur == None:
			return None

		### 1ere detection
		if capteur.getHorodatage() == None:
			### Set horodatage
			capteur.setHorodatageToNow()

			### return
			return capteur

		### Deja découvert, compare horodatage
		if self.isTooSoon(capteur) == True:
			return None

		### Set new horodatage et retn l'objet
		capteur.setHorodatageToNow()

		return capteur

	### Is fresh : comparaison horodatage
	def isTooSoon(self, capteur):
		"""
			Indique si l'horodatage est trop récent pour que l'on envois 1 mail
		"""
		if datetime.now() - capteur.getHorodatage() >= self.__frequence:
			return False
		return True

	### import data filtre from dict
	def _initFromConfiguration(self, configuration):
		"""
			Construit les attributs de l'objet selon 1 dictionnnaire formatter
		"""
		### frequence
		frequence = configuration["data"].get("frequence")
		if frequence == None:
			self.__frequence = timedelta(seconds=60)
		else:
			self.__frequence = timedelta(seconds=int(frequence))

		### capteurs
		for key in configuration["capteurs"]:
			### Modifie le format settings ?
			liste = json.loads(configuration["capteurs"][key])
			### Check validité avant insertion
			if Capteur.isMacValid(liste[0]) == False:
				continue
			self.__capteurs[liste[0]] = Capteur(liste[0], liste[1])








