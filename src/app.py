"""
Nom ......... : app.py
Role ........ : Serveur qui retourne 1 configuration à 1 client
Auteur ...... : Onésime
Version ..... : V1.1 du 31/3/2017
"""
### import
from flask import Flask, render_template, request, Response, url_for
from flask_restful import Resource, Api, reqparse

### Loader
from src.loader import Loader

### Obj loader
loader = Loader()
dico = loader.loadConfig("settings.ini")

### app serveur qui permet d'obtenir la configuration
app = Flask(__name__)
api = Api(app)

### Permet d'obtenir la config
class Configuration(Resource):
	"""
		Classe web permettant de retourner la configuration présente sur le serveur
	"""
	
	def __init__(self):
		pass

	### Get la configuration en JS
	def get(self):
		"""
			Fonction retournant la configuration en réponse d'une requête HTTP GET
		"""
		return dico

api.add_resource(Configuration, '/configuration')


### Run le serveur
if __name__ == "__main__":
	app.run()














