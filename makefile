SRC = $(wildcard src/*.py)
INT = $(SRC:/=.)
FILES= $(INT:.py=)

.PHONY: doc

doc:
	mkdir -p doc
	pydoc3 -w $(INT) #`find semlm -name '*.py'`
	mv *.html doc
